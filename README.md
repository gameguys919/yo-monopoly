![alt text](https://gitlab.com/gameguys919/yo-monopoly/-/raw/unnecessary/pics/monopoly.png)

# Just build your own

To get to the point of why I created this: some time ago i played Monopoly with a couple of friends and i guess we had a couple of drinks at this point. One of us had the great idea if its possible to create an own Monopoly'ish game of our best memmories and stuff like that. So the idea was born and in this night i started to look whats up in the internet, but i did not find any template which gave me the feeling of looking like a Real Monopoly game 
(i want our own game to look like its an original game on the first sight) so I started to create my own files (which was a pain in the ass to do).

## Getting started
_Important note before you start you need smth like 70 - 80 gb of free Storage for Photoshop to work properly and for all the files_

To make it easy for you to get started with the provided files you should use an instance of Photoshop to edit all the files Provided in this repo.

To get started just clone the repo or download the files:

```
https://gitlab.com/gameguys919/yo-monopoly/-/archive/main/yo-monopoly-main.zip?path=Monopoly%20files
```
or in terminal:
```
git clone https://gitlab.com/gameguys919/yo-monopoly.git
```
To do:
- [ ] **Modify main board as you wish**
    - [ ] Create **16** **Chance** and **16 Community Chest** cards
    - [ ] Create **22** individual **street cards** and **streets for the main board**
        - [ ] **2** brown 
        - [ ] **3** light blue
        - [ ] **3** pink
        - [ ] **3** orange
        - [ ] **3** red
        - [ ] **3** yellow
        - [ ] **3** green
        - [ ] **2** dark blue
    - [ ] create the **4 Railroads** as cards and fields for the main board
    - [ ] create **2** tax fields for the main board
    - [ ] create **2** fields **1** for the **Chance-** and **1** for the **Community Chest-** cards
    - [ ] create the **GO** field 
    - [ ] create the **Prison** field
    - [ ] create the **Free Parking** field
    - [ ] create the **go to jail** field
- [ ] **Modify the banknotes as you wish**
- [ ] **Prepare everything for printing**

## Overview:
**_Please note that on most Windows PC's you have to install the font i used (you will get a notification from Photoshop if the font is not installed). If needed just go in the tools directory and double click the .ttc file_**

**_`Important: you have to install the light and bold version of the font`_**




## Banknotes
- to change the color of the different banknotes select multiplicate es effect for the banknote level and then just change the color of the second level as you like it:
![alt text](https://gitlab.com/gameguys919/yo-monopoly/-/raw/unnecessary/pics/changecolorofbanknote.png)
![alt text](https://gitlab.com/gameguys919/yo-monopoly/-/raw/unnecessary/pics/changecolorofbanknote2.png)

## Prepare the Printfiles 

## Roadmap
- Im still cloning an original instructions file which you can use as a preset when im done

## Authors and License
I created all the files on my own and it took a lot of time to do this you can use them as you wish but please dont sell them or anything like that
(Out of copyright issues its anyways kind of impossible to sell them because i used original Monopoly graphics)

## Project status

-----
